package com.chanho.jobplanet.model

data class JobPlanetItem(
    var ranking: Int,
    var cell_type: String,
    var interview_difficulty: Double,
    var name: String,
    var salary_avg: Int,
    var web_site: String,
    var logo_path: String,
    var interview_question: String,
    var company_id: Int,
    var has_job_posting: String,
    var rate_total_avg: Double,
    var industry_id: Int,
    var review_summary: String,
    var type: String,
    var industry_name: String,
    var simple_desc: String,
    var cons:String,
    var days_ago:Int,
    var pros:String,
    var occupation_name:String,
    var date:String,
    var count: Int,
    var themes: List<ThemeItem>
)
