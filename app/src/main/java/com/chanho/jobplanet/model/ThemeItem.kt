package com.chanho.jobplanet.model

data class ThemeItem(
    var color:String,
    var cover_image:String,
    var id:Int,
    var title:String
)
