package com.chanho.jobplanet.retrofit

import com.chanho.jobplanet.model.ResModel
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitJobPlanetService {

    @GET("test_data.json")
    fun getJobPlanetList(
    ): Single<Response<ResModel>>
}