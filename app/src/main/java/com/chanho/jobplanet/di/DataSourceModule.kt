package com.chanho.basic.di

import com.chanho.basic.repository.RemoteDataSource
import com.chanho.basic.repository.RemoteDataSourceImpl
import com.chanho.jobplanet.retrofit.RetrofitJobPlanetService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DataSourceModule {

    @Singleton
    @Provides
    fun provideRemoteDataSource(
        retrofitJobPlanetService: RetrofitJobPlanetService
    ): RemoteDataSourceImpl {
        return RemoteDataSource(retrofitJobPlanetService)
    }
}