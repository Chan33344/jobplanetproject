package com.chanho.jobplanet

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ChanhoApplication :Application()