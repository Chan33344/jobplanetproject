package com.chanho.basic.repository

import com.chanho.jobplanet.model.ResModel
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class Repository
constructor(
    private val remoteDataSource: RemoteDataSourceImpl
) : RepositoryImpl {
    override fun getJobPlanetList(): Single<Response<ResModel>> {
        return remoteDataSource.onJobPlanetListCall()
            .subscribeOn(Schedulers.io())
    }
}
