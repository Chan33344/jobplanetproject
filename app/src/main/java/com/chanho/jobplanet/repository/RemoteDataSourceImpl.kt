package com.chanho.basic.repository

import com.chanho.jobplanet.model.ResModel
import io.reactivex.Single
import retrofit2.Response

interface RemoteDataSourceImpl {
    //NAVER MOVIE LIST
    fun onJobPlanetListCall(): Single<Response<ResModel>>

}