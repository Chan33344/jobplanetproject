package com.chanho.basic.repository

import com.chanho.jobplanet.model.ResModel
import com.chanho.jobplanet.retrofit.RetrofitJobPlanetService
import io.reactivex.Single
import retrofit2.Response

class RemoteDataSource(
    private val retrofitJobPlanetService: RetrofitJobPlanetService
) : RemoteDataSourceImpl {
    override fun onJobPlanetListCall(): Single<Response<ResModel>> {
        return retrofitJobPlanetService.getJobPlanetList()
    }
}