package com.chanho.jobplanet.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chanho.jobplanet.databinding.ActivityMainBinding
import com.chanho.jobplanet.model.JobPlanetItem

class MainAdapter(val viewModel: MainViewModel) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: List<JobPlanetItem> = ArrayList<JobPlanetItem>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            case
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }

    override fun getItemViewType(position: Int): Int = when (items[position].cell_type) {
        "CELL_TYPE_COMPANY" -> 1
        "CELL_TYPE_REVIEW" -> 2
        "CELL_TYPE_HORIZONTAL_THEME" -> 3
        else -> 4
    }

    override fun getItemCount(): Int = items.size


}