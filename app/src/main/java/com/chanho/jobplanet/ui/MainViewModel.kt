package com.chanho.jobplanet.ui

import android.annotation.SuppressLint
import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chanho.basic.repository.Repository
import com.chanho.jobplanet.model.JobPlanetItem
import io.reactivex.android.schedulers.AndroidSchedulers

class MainViewModel
@ViewModelInject constructor(private val repository: Repository):ViewModel(){
    private val _itemList = MutableLiveData<List<JobPlanetItem>>()
    val itemList :LiveData<List<JobPlanetItem>> = _itemList

    @SuppressLint("CheckResult")
    fun getJobPlanetList(){
        repository.getJobPlanetList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({data ->
                System.out.println(data.body())
                _itemList.value = data.body()?.items
            },{
                Log.e("error",it.toString())
            })
    }

}