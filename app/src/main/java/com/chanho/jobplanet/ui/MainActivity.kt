package com.chanho.jobplanet.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.observe
import com.chanho.jobplanet.R
import com.chanho.jobplanet.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val viewModel:MainViewModel by viewModels()
    private lateinit var binding:ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        with(binding){
            vm = viewModel
        }
        viewModel.getJobPlanetList()
        onObserve()
    }

    private fun onObserve(){
        viewModel.itemList.observe(this,{

        })
    }
}